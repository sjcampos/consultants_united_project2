﻿# Consultants_united_Project2

## Introducción

La empresa Grupo Innova S. A ha sido contratada con el propósito de diseñar e implementar indicadores clave de desempeño mediante una investigación de la metodología adecuada para la creación de estos respetando los atributos smart; estos kpis se requieren para poder monitorear de manera exitosa información de temas relacionados con ventas mensuales, rendimiento de sucursales, etc. Todo esto con la finalidad de poder aportar argumentos importantes en los cuales basar decisiones estratégicas

## Herramientas utilizadas

1. Power BI

2. Visual Studio 2017

3. SQL Server

4. SQL Server Managment Studio


## Estructura y rutas de repositorio

Seguidamente se listarán los links para accesar a diferentes archivos dentro de la carpeta del repositorio.

 - [Link del repositorio](https://gitlab.com/sjcampos/consultants_united_project2.git)
 - [Documentación](/Documents/Proyecto_2.pdf)
 - [KPIS](/Documents/KPIs_Grupo_1.pdf)
 - [Proyecto en Power BI](/PowerBI/Project.pbix)





